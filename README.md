# Dhumbal - Nepali Card Game

## Project Overview

Dhumbal is a Nepali card game. This repository contains the releases and a Docker Compose configuration file (`docker-compose.yml`) for deploying the Dhumbal project.

## Prerequisites

Before applying the Docker Compose file, make sure to update the CORS allowed origins on the `dhumbal-api` service environment. Open the `docker-compose.yml` file and locate the `dhumbal.security.corsAllowedOrigins` environment variable. Update its value with the desired CORS origins.

Example:

```yaml
services:
  dhumbal-api:
    environment:
      - dhumbal.security.corsAllowedOrigins: "http://localhost,http://<IP>"

```

## Deployment

To deploy the Dhumbal project, follow these steps:

1. Update CORS allowed origins as mentioned in the Prerequisites section.
2. Run the following command in the terminal:

```bash
docker compose up -d
```
This will start the Dhumbal project and its associated services.

## Support and Contribution

If you encounter any issues or have suggestions for improvement, feel free to open an issue in this repository. Contributions are welcome!

Happy gaming! 🎉
